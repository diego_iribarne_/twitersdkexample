package jmonlabs.com.twitterintegration;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;

import com.twitter.sdk.android.Twitter;
import com.twitter.sdk.android.core.TwitterAuthConfig;

import io.fabric.sdk.android.Fabric;
import jmonlabs.com.twitterintegration.home.HomeFragment;
import jmonlabs.com.twitterintegration.login.LoginFragment;

public class MainActivity extends AppCompatActivity {

    // Note: Your consumer key and secret should be obfuscated in your source code before shipping.
    private static final String TWITTER_KEY = "1T8KY3Yr15H0KT9DSiPUf1I1r";
    private static final String TWITTER_SECRET = "GUXcgjDq07WRX03W2Aft2ikKy76TmBCpkJaQYxS75zF9pLIqKm";
    private static final String LOGIN_FRAGMENT_TAG = "loginFragment";
    private static final String HOME_FRAGMENT_TAG = "homeFragment";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        TwitterAuthConfig authConfig = new TwitterAuthConfig(TWITTER_KEY, TWITTER_SECRET);
        Fabric.with(this, new Twitter(authConfig));
        setContentView(R.layout.activity_main);
        if (Twitter.getSessionManager().getActiveSession() == null)
            getSupportFragmentManager().beginTransaction()
                    .replace(R.id.list_container, LoginFragment.newInstance(), LOGIN_FRAGMENT_TAG).commit();
        else {
            if (savedInstanceState == null) {
                getSupportFragmentManager().beginTransaction()
                        .replace(R.id.list_container, HomeFragment.newInstance()).commit();
            }
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        Fragment fragment = getSupportFragmentManager().findFragmentByTag(LOGIN_FRAGMENT_TAG);
        if (fragment != null) {
            fragment.onActivityResult(requestCode, resultCode, data);
        }
    }
}
