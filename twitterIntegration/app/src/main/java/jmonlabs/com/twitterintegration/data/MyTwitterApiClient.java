package jmonlabs.com.twitterintegration.data;

import com.twitter.sdk.android.core.Callback;
import com.twitter.sdk.android.core.TwitterApiClient;
import com.twitter.sdk.android.core.TwitterSession;
import com.twitter.sdk.android.core.models.Search;

import retrofit.http.GET;
import retrofit.http.Query;

public class MyTwitterApiClient extends TwitterApiClient implements ApiClient {
    public MyTwitterApiClient(TwitterSession session) {
        super(session);
    }

    public void getTweets(String query, Callback<Search> callback) {
        getService(SearchService.class).tweets(query, callback);
    }
}

// example users/show service endpoint
interface SearchService {
    @GET("/1.1/search/tweets.json")
    void tweets(@Query("q") String query,
                Callback<Search> cb);
}