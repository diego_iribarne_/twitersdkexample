package jmonlabs.com.twitterintegration.home;

import com.twitter.sdk.android.core.Callback;
import com.twitter.sdk.android.core.Result;
import com.twitter.sdk.android.core.TwitterException;
import com.twitter.sdk.android.core.models.Search;

import jmonlabs.com.twitterintegration.data.ApiClient;

/**
 * Created by diegoiribarne on 29/6/16.
 */
public class HomePresenter implements HomeContract.UserActionsListener {
    ApiClient api;
    HomeContract.View view;

    public HomePresenter(ApiClient api, HomeContract.View view) {
        this.api = api;
        this.view = view;
    }

    @Override
    public void searchTweets(String tag) {
        view.showProgress(true);
        api.getTweets(tag, new Callback<Search>() {
            @Override
            public void success(Result<Search> result) {
                view.showProgress(false);
                view.showTweets(result.data.tweets);
            }

            @Override
            public void failure(TwitterException exception) {

            }
        });
    }
}
