package jmonlabs.com.twitterintegration.data;

import com.twitter.sdk.android.core.Callback;
import com.twitter.sdk.android.core.models.Search;

/**
 * Created by diegoiribarne on 29/6/16.
 */
public interface ApiClient {
    void getTweets(String query, Callback<Search> callback);
}
